package com.example.correctthis.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.correctthis.R;
import com.example.correctthis.menus.MainActivity;
import com.example.correctthis.objects.Class;

import java.util.ArrayList;

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.ClassViewHolder> {

    private ArrayList<Class> classList;
    private Context mContext;
    public OnItemClickListener mListener;


    public ClassAdapter(ArrayList<Class> cList,MainActivity mContext) {
        this.classList = cList;
        this.mContext = mContext;
    }

    public static class ClassViewHolder extends RecyclerView.ViewHolder {

       public TextView className;
       public TextView classNumberOfStudents;
       public TextView subject;

        public ClassViewHolder(@NonNull View itemView, final ClassAdapter.OnItemClickListener listener) {
            super(itemView);


            className = itemView.findViewById(R.id.tv_className);
            classNumberOfStudents = itemView.findViewById(R.id.tv_numberOfStudents);
            subject = itemView.findViewById(R.id.tv_subject);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener !=null){
                        int positon = getAdapterPosition();
                        if(positon != RecyclerView.NO_POSITION){
                            listener.onItemClick(positon);
                        }
                    }
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClick(int position);

    }

    public void setOnItemClickListener(ClassAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    @NonNull
    @Override
    public ClassViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.class_layout,parent,false);
        ClassAdapter.ClassViewHolder f = new ClassAdapter.ClassViewHolder(v,mListener);
        return f;
    }

    @Override
    public void onBindViewHolder(@NonNull ClassViewHolder holder, int position) {
        Class s = classList.get(position);
        holder.classNumberOfStudents.setText(s.getNumberOfStudents());
        holder.className.setText(s.getClass_name());
        holder.subject.setText(s.getSubject());
        //holder.classList.setText(c.getName());


    }

    @Override
    public int getItemCount() {
        return classList.size();
    }


}
