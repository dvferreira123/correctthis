package com.example.correctthis.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.correctthis.objects.Question;
import com.example.correctthis.R;

import java.util.ArrayList;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder> {

    private ArrayList<Question> questionList;
    private Context mContext;
    public OnItemClickListener mListener;


    public QuestionAdapter(Context context,ArrayList<Question> questionList) {
        this.questionList = questionList;
        this.mContext=context;
    }

    public static class QuestionViewHolder extends RecyclerView.ViewHolder {

        public TextView questionNumber;
        public TextView questionGrade;


        public QuestionViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            questionNumber = itemView.findViewById(R.id.tv_questionNumber);
            questionGrade = itemView.findViewById(R.id.tv_questionGrade);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener !=null){
                        int positon = getAdapterPosition();
                        if(positon != RecyclerView.NO_POSITION){
                            listener.onItemClick(positon);
                        }
                    }
                }
            });


        }
    }


    public interface OnItemClickListener{
        void onItemClick(int position);

    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }


    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_layout,parent,false);
        QuestionViewHolder f = new QuestionViewHolder(v,mListener);
        return f;
    }



    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder holder, int position) {
        Question q = questionList.get(position);
        holder.questionNumber.setText(q.getQuestionNumber());
        holder.questionGrade.setText(q.getQuestionGrade());


    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }





}
