package com.example.correctthis.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.correctthis.objects.Student;
import com.example.correctthis.R;

import java.util.ArrayList;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentViewHolder> {

    private ArrayList<Student> studentList;
    private Context mContext;
    public OnItemClickListener mListener;

    public StudentAdapter(ArrayList<Student> studentList, Context mContext) {
        this.studentList = studentList;
        this.mContext = mContext;
    }

    public static class StudentViewHolder extends RecyclerView.ViewHolder {

       public TextView studentName;

        public StudentViewHolder(@NonNull View itemView, final StudentAdapter.OnItemClickListener listener) {
            super(itemView);


            studentName = itemView.findViewById(R.id.tv_studentName);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener !=null){
                        int positon = getAdapterPosition();
                        if(positon != RecyclerView.NO_POSITION){
                            listener.onItemClick(positon);
                        }
                    }
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClick(int position);

    }

    public void setOnItemClickListener(StudentAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    @NonNull
    @Override
    public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_layout,parent,false);
        StudentAdapter.StudentViewHolder f = new StudentAdapter.StudentViewHolder(v,mListener);
        return f;
    }

    @Override
    public void onBindViewHolder(@NonNull StudentViewHolder holder, int position) {
        Student s = studentList.get(position);
        holder.studentName.setText(s.getName());

    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }


}
