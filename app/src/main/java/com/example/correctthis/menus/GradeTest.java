package com.example.correctthis.menus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.correctthis.adapters.QuestionAdapter;
import com.example.correctthis.objects.Question;
import com.example.correctthis.R;

import java.util.ArrayList;
import java.util.Locale;

public class GradeTest extends AppCompatActivity {

    public ArrayList<Question> questions;
    public int numberOfQuestions;
    public int selectedQuestionPosition;
    private String TAG = "DEBUG: ";

    private TextView tv_currentQuestion;
    private TextView tv_finalGrade;


    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private QuestionAdapter mAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade_test);

        tv_currentQuestion = findViewById(R.id.tv_currentQuestion);
        tv_finalGrade = findViewById(R.id.tv_finalGrade);

        //createQuestionList();
        Intent i = getIntent();

        buildRecyclerView();
        //Insert and Removing Questions


        ;

    }

    public void insertQuestion(int position){
        questions.add(position,new Question("0","0"));
        mAdapter.notifyItemInserted(position);
    }

    public void removeQuestion(int position){
        questions.remove(position);
        mAdapter.notifyItemRemoved(position);


    }

    public void changeGrade (int position,String gradeText){
        questions.get(position).setQuestionGrade(gradeText);

        mAdapter.notifyItemChanged(position);

    }

    public void changeQuestionNumber(int postion,String questionText){
        questions.get(postion).setQuestionNumber(questionText);
        mAdapter.notifyItemChanged(postion);
    }


    private void buildRecyclerView() {
        recyclerView = findViewById(R.id.rv_listOfQuestions);
        //recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mAdapter = new QuestionAdapter(this,questions);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);


        mAdapter.setOnItemClickListener(new QuestionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
               // changeGrade(position,"20");
               // changeQuestionNumber(position,"Q." + position);
                selectedQuestionPosition = position;
                tv_currentQuestion.setText(String.valueOf(position + 1));


            }



        });
    }

    private void createQuestionList() {
        Intent intent = getIntent();
        numberOfQuestions = intent.getIntExtra("numberOfQuestions",0);
        Log.v("QUESTIONS:::", String.valueOf(numberOfQuestions));
        questions = new ArrayList<>();
        for(int i = 1;i <= numberOfQuestions;i++){
            questions.add(new Question(String.valueOf(i),"0"));
        }

    }

    private void generateFinalGrade(){
        String finalGrade = "0";
        int fGrade = 0;
        for (Question q: questions) {
            fGrade = fGrade + Integer.parseInt(q.getQuestionGrade());
        }

        finalGrade = String.valueOf(fGrade);

        tv_finalGrade.setText(finalGrade);
    }



    public void getSpeechInput(View view) {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Toast.makeText(this,result.get(0),Toast.LENGTH_LONG).show();


                    if(result.get(0).equals("corrigir")){
                        generateFinalGrade();
                    }else{
                        questions.get(selectedQuestionPosition);
                        changeGrade(selectedQuestionPosition,result.get(0));
                    }
                }
                break;
        }
    }
}
