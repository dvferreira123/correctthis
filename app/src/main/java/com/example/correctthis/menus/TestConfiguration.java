package com.example.correctthis.menus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.correctthis.objects.Question;
import com.example.correctthis.objects.Test;
import com.example.correctthis.R;

import java.util.List;

public class TestConfiguration extends AppCompatActivity {


    public String grade;
    public List<Question> qs;

    public EditText et_numberOfQuestions;
    public Button b_confirmNumberOfQuestions;
    public int numberOfQuestions = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_configuration);

        //Create new test
        Test t = new Test("Test de Matemática 1º Ano",grade,qs);

        //provide de Number of questions

        et_numberOfQuestions = findViewById(R.id.et_questions);
        b_confirmNumberOfQuestions = findViewById(R.id.b_confirm);

        b_confirmNumberOfQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numberOfQuestions = Integer.valueOf(et_numberOfQuestions.getText().toString());

                //Create new Intent to grade attribution
                Intent s = new Intent(TestConfiguration.this,StudentsMenu.class);
                s.putExtra("numberOfQuestions",numberOfQuestions);
                startActivity(s);




            }
        });

    }



}
