package com.example.correctthis.menus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.correctthis.adapters.StudentAdapter;
import com.example.correctthis.DatabaseHelper;
import com.example.correctthis.objects.Question;
import com.example.correctthis.objects.Student;
import com.example.correctthis.objects.Test;
import com.example.correctthis.R;

import java.util.ArrayList;
import java.util.Locale;

public class StudentsMenu extends AppCompatActivity {


    public ArrayList<Student> students;
    public DatabaseHelper myDb;
    public int numberOfStudents;
    public int numberOfQuestions;
    public int selectedStudentPosition;

    public ArrayList<Question> questionsList;


    private Button b_InsertStudent;
    private Button b_RemoveStudent;
    public int studentCounter = 0;


    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private StudentAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_menu);

        myDb = new DatabaseHelper(this);
        b_InsertStudent = findViewById(R.id.b_InsertStudent);
        b_RemoveStudent = findViewById(R.id.b_RemoveStudent);


        Intent i = getIntent();

        //se vier de selecionar uma class
        if(!i.getStringExtra("class_name_pkey").equals(null)){
            Cursor res = myDb.getStudentData(i.getStringExtra("class_name_pkey"));
            fetchStudentList(res);

        }else{
            numberOfQuestions = i.getIntExtra("numberOfQuestions",0);
            students = new ArrayList<>();
            createQuestionList();
        }


        buildRecyclerView();
    }


    public void addStudent(int position,String studentName, Test t){
        Log.v("POS:",String.valueOf(position));
        Student x = new Student(studentName,t);
        students.add(position,x);
        mAdapter.notifyItemInserted(position);
        myDb.insertStudentData(studentName,"0","");


    }

    public void removeStudent(int position){
        students.remove(position);
        mAdapter.notifyItemRemoved(position);


    }

    private void createQuestionList() {

        Log.v("QUESTIONS:::", String.valueOf(numberOfQuestions));
        questionsList = new ArrayList<>();

        for(int i = 1;i <= numberOfQuestions;i++){
            questionsList.add(new Question(String.valueOf(i),"0"));
        }

    }

    private void fetchStudentList(Cursor listOfQuestionsCursor) {

        students = new ArrayList<>();

        for(int i = 1;i <= listOfQuestionsCursor.getCount();i++){
          //  students.add(i,new Student(listOfQuestionsCursor.getString(0),""));
        }

    }









    private void buildRecyclerView() {
        recyclerView = findViewById(R.id.rv_listOfStudents);
        //recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mAdapter = new StudentAdapter(students,this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);


        mAdapter.setOnItemClickListener(new StudentAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                // changeGrade(position,"20");
                // changeQuestionNumber(position,"Q." + position);
                selectedStudentPosition = position;

                //Criar nova Intent para class GradTest
                Intent s = new Intent(StudentsMenu.this,GradeTest.class);
                s.putExtra("numberOfQuestions",numberOfQuestions);
                startActivity(s);
            }
        });
    }
    public void getSpeechInput(View view) {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Toast.makeText(this,result.get(0),Toast.LENGTH_LONG).show();


                    //Create a new test and student


                    Test t = new Test("Test de Matemática 1º Ano","0",questionsList);

                    addStudent(studentCounter,result.get(0),t);
                    studentCounter = studentCounter +1;



                }
                break;
        }
    }
}
