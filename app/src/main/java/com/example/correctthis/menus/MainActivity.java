package com.example.correctthis.menus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.correctthis.DatabaseHelper;
import com.example.correctthis.R;
import com.example.correctthis.adapters.ClassAdapter;
import com.example.correctthis.adapters.StudentAdapter;
import com.example.correctthis.objects.Class;
import com.example.correctthis.objects.Question;
import com.example.correctthis.objects.Student;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public Button addTest;
    DatabaseHelper myDb;


    public ArrayList<Class> classList;
    public int numberOfStudents;
    public int numberOfClasses;
    public int selectedClassPosition;

    public ArrayList<Student> studentList;





    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ClassAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

///CREATE DATABSE
        myDb = new DatabaseHelper(this);


        createClassList();
        buildRecyclerView();
        myDb.insertClassData("Test Class 1", "Matemática");



        addTest = findViewById(R.id.b_AddTest);
        addTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TestConfiguration.class);
                startActivity(intent);

            }
        });
    }


    public void getClassData(){
        Cursor res = myDb.getClassData();
        if(res.getCount() == 0){
            Toast.makeText(this,"LIST IS EMPTY",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this,"LIST:" + res.getCount(),Toast.LENGTH_LONG).show();

            StringBuffer buffer = new StringBuffer();
            while (res.moveToNext()){
                //Get all data and display it on the recycle view
            }
        }
    }




    private void createClassList() {

        Log.v("Classes:::", String.valueOf(numberOfClasses));
        classList = new ArrayList<>();

        //get DAta from db
        Cursor c_data = myDb.getClassData();

        Log.v("CLASSES:",String.valueOf(c_data.getCount()));
        if(c_data.getCount() > 0){
            while(c_data.moveToNext()){
                String name = c_data.getString(1);
                String subject = c_data.getString(2);
                String numberOfStudents = c_data.getString(0);
                classList.add(new Class(name,subject,numberOfStudents));
            }
        }





    }


    private void buildRecyclerView() {
        recyclerView = findViewById(R.id.rv_classlistView);
        //recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mAdapter = new ClassAdapter(classList,this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);


        mAdapter.setOnItemClickListener(new ClassAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                // changeGrade(position,"20");
                // changeQuestionNumber(position,"Q." + position);
                selectedClassPosition = position;
                String class_name_pkey = classList.get(position).getClass_name();

                Intent t = new Intent(MainActivity.this,StudentsMenu.class);
                t.putExtra("class_name_pkey",class_name_pkey);
                startActivity(t);


            }
        });
    }






}
