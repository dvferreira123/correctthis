package com.example.correctthis.objects;

public class Class {

    public String class_name;
    public String subject;
    public String numberOfStudents;

    public Class(String class_name, String subject, String numberOfStudents) {
        this.class_name = class_name;
        this.subject = subject;
        this.numberOfStudents = numberOfStudents;
    }


    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getNumberOfStudents() {
        return numberOfStudents;
    }

    public void setNumberOfStudents(String numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }
}
