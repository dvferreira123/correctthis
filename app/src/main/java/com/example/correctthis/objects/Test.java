package com.example.correctthis.objects;

import java.util.List;

public class Test {


    public Question q;
    public String name;
    public String grade;
    public List<Question> questionsList;



    public Test(String name,String grade, List<Question> questionsList) {
        this.grade = grade;
        this.questionsList = questionsList;
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public List<Question> getQuestionsList() {
        return questionsList;
    }

    public void setQuestionsList(List<Question> questionsList) {
        this.questionsList = questionsList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
