package com.example.correctthis.objects;

import android.util.Log;

public class Question {

    public String questionNumber;
    public String questionGrade;


    public Question(String questionNumber, String questionGrade){
        this.questionGrade = questionGrade;
        this.questionNumber = questionNumber;
    }



    public String getQuestionNumber() {
        return questionNumber;
    }

    public String getQuestionGrade() {
        return questionGrade;
    }

    public void setQuestionNumber(String questionNumber) {
        this.questionNumber = questionNumber;
    }

    public void setQuestionGrade(String grade) {
        Log.v("Question1",this.questionGrade);
        Log.v("question2",grade);
        this.questionGrade = grade;
        Log.v("Question1",this.questionGrade);


    }
}
