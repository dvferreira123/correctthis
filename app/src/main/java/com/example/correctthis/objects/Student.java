package com.example.correctthis.objects;

public class Student {

    public String name;
    public Test test;

    public Student(String _name,Test _test){
        this.name = _name;
        this.test = _test;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }
}
