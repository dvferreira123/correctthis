package com.example.correctthis;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME ="School.db";

    //Class table
    public static final String CLASS_TABLE_NAME = "classes_table";
    public static final String COL_1_C ="ID_CLASS";
    public static final String COL_2_C ="CLASS_NAME";
    public static final String COL_3_C ="SUBJECT";
    public static final String COL_4_C ="NUMBER_OF_STUDENTS";


    //Student table
    public static final String STUDENT_TABLE_NAME = "students_table";
    public static final String COL_1_S ="ID_STUDENT";
    public static final String COL_2_S ="FULL_NAME";
    public static final String COL_3_S ="MARKS";
    public static final String COL_4_S ="ID_CLASS_FK";

    //Test Table
    public static final String TEST_TABLE_NAME = "tests_table";
    public static final String COL_1_T ="ID_TEST";
    public static final String COL_2_T ="TEST_NAME";
    public static final String COL_3_T ="ID_STUDENT_FK";

    //Questions table
    public static final String QUESTION_TABLE_NAME = "questions_table";
    public static final String COL_1_Q ="ID_QUESTION";
    public static final String COL_2_Q ="NAME";
    public static final String COL_3_Q ="GRADE";
    public static final String COL_4_Q ="ID_TEST_FK";

    public static final String CREATE_DATE = "CREATE_DATE";
    public static final String LAST_UPDATE_DATE = "LAST_UPDATE_DATE";

    public Cursor res;




///////////////////////////////////////////////////////////////////////////////////////7


    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME,null,1);
    }

    //CREATE ALL TABLES/////////////////
    @Override
    public void onCreate(SQLiteDatabase db) {
        //Create classes table
        db.execSQL(" CREATE TABLE " + CLASS_TABLE_NAME + " (" +
                COL_2_C + " TEXT PRIMARY KEY NOT NULL, " +
                COL_3_C + " TEXT NOT NULL, " +
                CREATE_DATE + " TEXT NOT NULL, " +
                LAST_UPDATE_DATE + " TEXT NOT NULL, " +
                COL_4_C + " TEXT NOT NULL);"
        );

        //create student table
        db.execSQL(" CREATE TABLE " + STUDENT_TABLE_NAME + " (" +
                COL_1_S + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_2_S + " TEXT NOT NULL, " +
                COL_3_S + " TEXT NOT NULL, " +
                CREATE_DATE + " TEXT NOT NULL, " +
                LAST_UPDATE_DATE + " TEXT NOT NULL, " +
                COL_4_S + " INTEGER,"
                + " FOREIGN KEY ("+COL_4_S+") REFERENCES "+CLASS_TABLE_NAME+"("+COL_2_C+"));"
        );

        //Create test table
        db.execSQL(" CREATE TABLE " + TEST_TABLE_NAME + " (" +
                COL_1_T + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_2_T + " TEXT NOT NULL, " +
                CREATE_DATE + " TEXT NOT NULL, " +
                LAST_UPDATE_DATE + " TEXT NOT NULL, " +
                COL_3_T + " INTEGER,"
                + " FOREIGN KEY ("+COL_3_T+") REFERENCES "+STUDENT_TABLE_NAME+"("+COL_1_S+"));"
        );

        //Create questiont table
        db.execSQL(" CREATE TABLE " + QUESTION_TABLE_NAME + " (" +
                COL_1_Q + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_2_Q + " TEXT NOT NULL, " +
                COL_3_Q + " TEXT NOT NULL, " +
                CREATE_DATE + " TEXT NOT NULL, " +
                LAST_UPDATE_DATE + " TEXT NOT NULL, " +
                COL_4_Q + " INTEGER,"
                + " FOREIGN KEY ("+COL_4_Q+") REFERENCES "+TEST_TABLE_NAME+"("+COL_1_T+"));"
        );

    }


    ///////////////DROP TABLES IF EXISTS////////////////////////////////
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(" DROP TABLE IF EXISTS " + CLASS_TABLE_NAME);
        db.execSQL(" DROP TABLE IF EXISTS " + STUDENT_TABLE_NAME);
        db.execSQL(" DROP TABLE IF EXISTS " + TEST_TABLE_NAME);
        db.execSQL(" DROP TABLE IF EXISTS " + QUESTION_TABLE_NAME);

        onCreate(db);
    }

    ///////////////INSERT CLASS/////////////////////////////////7
    public boolean insertClassData(String class_name, String subject){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2_C,class_name);
        contentValues.put(COL_3_C,subject);
        long result = db.insert(CLASS_TABLE_NAME,null,contentValues);

        if(result == -1){
            return false;
        }else{
            return true;
        }
    }

    ///////////////INSERT STUDENT/////////////////////////////////7
    public boolean insertStudentData(String full_name, String marks,String class_id_FK){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2_S,full_name);
        contentValues.put(COL_3_S,marks);
        contentValues.put(COL_4_S,class_id_FK);
        long result = db.insert(STUDENT_TABLE_NAME,null,contentValues);

        if(result == -1){
            return false;
        }else{
            return true;
        }
    }
    ///////////////INSERT TEST/////////////////////////////////7
    public boolean insertTestData(String test_name, String id_student_fk){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2_T,test_name);
        contentValues.put(COL_3_T,id_student_fk);
        long result = db.insert(TEST_TABLE_NAME,null,contentValues);

        if(result == -1){
            return false;
        }else{
            return true;
        }
    }
    ///////////////INSERT QUESTION/////////////////////////////////7
    public boolean insertQuestionData(String name, String grade,String test_id_FK){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2_Q,name);
        contentValues.put(COL_3_Q,grade);
        contentValues.put(COL_4_Q,test_id_FK);
        long result = db.insert(QUESTION_TABLE_NAME,null,contentValues);

        if(result == -1){
            return false;
        }else{
            return true;
        }
    }


    //GET DATA////////////////////////////////////////////////////////
    public Cursor getClassData(){
        SQLiteDatabase db = this.getWritableDatabase();
        res = db.rawQuery("select * from " +  CLASS_TABLE_NAME,null);

        return res;
    }


    //GET ALL DATA
    public Cursor getStudentData(String class_name){
        SQLiteDatabase db = this.getWritableDatabase();
        res = db.rawQuery("select * from " +  STUDENT_TABLE_NAME + "where CLASS_NAME = " + class_name,null);

        return res;
    }

    //GET ALL DATA
    public Cursor getTestData(){
        SQLiteDatabase db = this.getWritableDatabase();
        res = db.rawQuery("select * from " +  TEST_TABLE_NAME,null);

        return res;
    }

    //GET ALL DATA
    public Cursor getQuestionData(){
        SQLiteDatabase db = this.getWritableDatabase();
        res = db.rawQuery("select * from " +  QUESTION_TABLE_NAME,null);

        return res;
    }
    //GET DATA////////////////////////////////////////////////////////



    //CUSTOM QUERYS
        public Cursor getAllData(String position){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " +  CLASS_TABLE_NAME + " where ID_CLASS ==" + position ,null);

        return res;
    }
}
